/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

global.config = JSON.parse(require('fs').readFileSync('./config.json'));

var requestjs = require('request');
var url = require('url');
var sha256 = require('js-sha256');

var controller = function(config){
    var self = this;
    
    self.baseurl = config.cnc.baseurl + config.cnc.token;
    
    this.nocncconnection = function() {
        console.error('$$ WARNING $$ | Connection to cnc server failed');
    };
    
    this.auth = function(username, password, callback) {
        
        console.log('Authentication request from user', username, 'with password "',password,'"');
        
        var hash = sha256(username + password);
        try {
            requestjs.post({
                    url: self.baseurl + '/auth',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({user: hash})
                },
                (err,httpResponse,body) => {

                    if (err) {
                        if (err.code === 'ECONNREFUSED') self.nocncconnection();
                        setTimeout(callback, 2000, false);
                    }
                    else if (JSON.parse(body).result === 'AUTH_ACCEPT') setImmediate(callback, true);
                    else setTimeout(callback, 500, false);
                }
            );
        } catch (e) {
            setTimeout(callback, 10*1000, false);
            console.log('Error while attempting to ask cnc server to authenticate user.\nError:', e);
        }

    };
    
    this.register = function() {
        try {
            requestjs.post({
                    url: self.baseurl + '/register',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(config.proxy)
                },
                (err,httpResponse,body) => {
                    if (err) {
                        if (err.code === 'ECONNREFUSED') self.nocncconnection();
                    }
                    else {
                        body = JSON.parse(body);
                        console.log('Registered on Control Server. Answer:', body);
                    }
                }
            );
        } catch (e) {
            console.error('Error while attempting to register on cnc server.\nError Message:', e);
        }

    };
    
    this.sendStats = function(userdatausage) {
        
        // calculate total data usage
        let totalusage = 0;
        for (var i in userdatausage) {
            totalusage += userdatausage[i];
        }
        
        var stats = {
            totalUsage: totalusage,
            users: userdatausage // {"unam:pass": int:bytes}
        };
        
        try {
            requestjs.post({
                    url: self.baseurl + '/stats',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(stats)
                },
                (err,httpResponse,body) => {
                    if (err) {
                        if (err.code === 'ECONNREFUSED') self.nocncconnection();
                    }
                }
            );
        } catch (e) {
            console.error('Error while attempting to send Stats to cnc server.\nError Message:', e);
        }
    };
    
    // Heartbeat
    setInterval(function() {
        requestjs.get(
            self.baseurl + '/heartbeat',
            (err,httpResponse,body) => {
                if (err) {
                    if (err.code === 'ECONNREFUSED') self.nocncconnection();
                }
            }
        );
    }, config.cnc.rates.heartbeat*1000);
};


const TGProxy = function(config, controller) {
    const socks5 = require('./simple-filter-socks.js');
    const ip = require('ip');
    
    const bytesToSize = function (bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        if (i === 0) return bytes + ' ' + sizes[i]; 
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    };

    var t_userConsumption = {};
    
    // regularly push the userstats to the cnc server
    setInterval(function(tempcons) {

        // send to cnc server
        controller.sendStats(tempcons);
        
        // reset cache
        for each(var key in tempcons) {
            delete tempcons[key];
        }
        
    }, config.cnc.rates.stats *1000, t_userConsumption);
    
    var server = socks5.createServer({
        authenticate : function (username, password, callback) {

            // verify username/password
            controller.auth(username, password, (authed) => {

                console.log('Authentication result for', username, '=>', authed);

                if (authed !== false) {
                    setImmediate(callback);
                } else {
                    setImmediate(callback, new Error('invalid credentials'));
                };
            });
        },

        hostFilter : function(host, callback) {

            if (ip.isV4Format(host.addr)) {
                var whitelist = config.whitelist.ipv4;
            } else if (ip.isV6Format(host.addr)) {
                var whitelist = config.whitelist.ipv6;
            } else {
                setImmediate(callback, false);; return;
            }

            found = (whitelist.find(e => {
                return ip.cidrSubnet(e).contains(host.addr);
            }) === undefined) ? false : true;

            setImmediate(callback, found);
        }
    });

    

    server.on('proxyData', function (info) {
        // set up user data cache if neccessairy
        if (t_userConsumption[info.user.hash] === undefined) 
            t_userConsumption[info.user.hash] = 0;
        
        // add data usage to cache
        t_userConsumption[info.user.hash] += info.data.length;
    });

    // When an error occurs connecting to remote destination
    server.on('proxyError', function (err) {
        console.error('unable to connect to remote server');
        console.error(err);
    });
   
    
    // start listening!
    let proxyport = config.proxy.port || 1080;
    server.listen(proxyport);
    
    // Tell the cnc server we are ready
    controller.register();
    
    // LOG
    console.log('Listening for incoming data on port',proxyport);
       
    
};

var proxy = new TGProxy(global.config, new controller(global.config));